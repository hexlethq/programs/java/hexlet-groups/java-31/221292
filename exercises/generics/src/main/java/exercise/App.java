package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class App {

    public static <T> List findWhere(List<Map<T, T>> books, Map<String, String> where) {
        ArrayList result = new ArrayList();
        for (Map book : books) {
            boolean containsCheck = true;
            for (Map.Entry<String, String> entry : where.entrySet()) {
                // get key
                String key = entry.getKey();
                // get value
                String value = entry.getValue();
                if (!book.get(key).equals(value)) {
                    containsCheck = false;
                }
            }
            if (containsCheck) {
                result.add(book);
            }
        }
        return result;
    }
}