package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        if (sentence.substring(sentence.length() - 1).equals("!")) {
            System.out.print(sentence.toUpperCase());
        }
        else{
            System.out.print(sentence.toLowerCase());
        }
    }
}
