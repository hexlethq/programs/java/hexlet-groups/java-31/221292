package exercise.controllers;

import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;

import java.util.List;

import exercise.domain.User;
import exercise.domain.query.QUser;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        List<User> users = new QUser()
                .orderBy()
                .id.asc()
                .findList();
        ctx.json(DB.json().toJson(users));
    }

    public void getOne(Context ctx, String id) {
        User user = new QUser()
                .id.equalTo(ctx.pathParamAsClass("id", Long.class).getOrDefault(null))
                .findOne();
        ctx.json(DB.json().toJson(user));
    }

    public void create(Context ctx) {
        String body = ctx.body();
        User user = DB.json().toBean(User.class, body);
        user.save();
        ctx.json(user);
    }

    public void update(Context ctx, String id) {
        User user = new QUser()
                .id.equalTo(ctx.pathParamAsClass("id", Long.class).getOrDefault(null))
                .findOne();
        String body = ctx.body();
        user.update();
    }

    ;

    public void delete(Context ctx, String id) {
        User user = new QUser()
                .id.equalTo(ctx.pathParamAsClass("id", Long.class).getOrDefault(null))
                .findOne();
        user.delete();
    }
}
