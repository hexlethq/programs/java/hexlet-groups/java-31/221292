package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

class App {

    public static String buildList(String[] arr) {
        if(arr.length == 0){
            return "";
        }
        StringBuilder res = new StringBuilder();
        res.append("<ul>" + '\n');
        for (String str : arr) {
            res.append("  " + "<li>").append(str).append("</li>").append('\n');
        }
        res.append("</ul>");
        return String.valueOf(res);
    }

    public static String getUsersByYear(String[][] users, int year) {
        String[] resultarr = new String[getNumOfOccurYear(users, year)];
        for (int i = 0, k = 0; k < resultarr.length; i++) {
            if (LocalDate.parse(users[i][1], DateTimeFormatter.ofPattern("y-MM-dd")).getYear() == year) {
                resultarr[k] = users[i][0];
                k++;
            }
        }
        return buildList(resultarr);
    }

    //Количество людей с указанным годом
    public static int getNumOfOccurYear(String[][] users, int year) {
        int j = 0;
        for (String[] user : users) {
            if (LocalDate.parse(user[1], DateTimeFormatter.ofPattern("y-MM-dd")).getYear() == year){
                j++;
            }
        }
        return j;
    }

    public static String getYoungestUser(String[][] users, String date) {
        ArrayList<Integer> indexs = getBeforeYearIndex(users, date);
        LocalDate youngest = LocalDate.parse(users[indexs.get(0)][1], DateTimeFormatter.ofPattern("y-MM-dd"));
        String resultstring = users[indexs.get(0)][0];
        for (int i = 1; i < indexs.size(); i++){
            if(LocalDate.parse(users[indexs.get(i)][1], DateTimeFormatter.ofPattern("y-MM-dd")).isAfter(youngest)){
                resultstring = users[indexs.get(i)][0];
            }
        }
        return resultstring;
    }

    public static ArrayList<Integer> getBeforeYearIndex(String[][] users, String date){
        ArrayList<Integer> indexs = new ArrayList<>();
        for (int i = 0; i < users.length; i++) {
            if(LocalDate.parse(users[i][1],
                    DateTimeFormatter.ofPattern("y-MM-dd")).isBefore(LocalDate.parse(date, DateTimeFormatter.ofPattern("dd MMM y", Locale.ENGLISH)))){
                indexs.add(i);
            }
        }
        return indexs;
    }
}

