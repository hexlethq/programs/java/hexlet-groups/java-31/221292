package exercise.controller;

import exercise.CityNotFoundException;
import exercise.model.City;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping("/")
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    @GetMapping(path = "cities/{id}")
    public Map<String, String> getWeather(@PathVariable long id) {
        City city = cityRepository.findById(id)
                .orElseThrow(() -> new CityNotFoundException("City not found"));
        return weatherService.getWeather(city).get();
    }

    @GetMapping(path = "search")
    public ArrayList<Map<String, String>> getWeather(@RequestParam(value = "name", required = false) String name) {
        ArrayList<Map<String, String>> result = new ArrayList<>();
        if (name == null) {
            Iterable<City> all = cityRepository.findByOrderByNameAsc();
            for (City city : all) {
                Map<String, String> rescity = new HashMap<>();
                rescity.put("temperature", weatherService.getWeather(city).get().get("temperature"));
                rescity.put("name", city.getName());
                result.add(rescity);
            }
            return result;
        }
        Iterable<City> all = cityRepository.findByNameStartsWithIgnoreCase(name);
        for (City city : all) {
            Map<String, String> rescity = new HashMap<>();
            if (city.getName().toLowerCase().startsWith(name.toLowerCase())) {
                rescity.put("temperature", weatherService.getWeather(city).get().get("temperature"));
                rescity.put("name", city.getName());
                result.add(rescity);
            }
        }
        return result;
    }
}

