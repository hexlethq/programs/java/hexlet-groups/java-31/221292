package exercise.repository;

import exercise.model.City;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {

    Iterable<City> findByNameStartsWithIgnoreCase(String name);
    Iterable<City> findByOrderByNameAsc();
}
