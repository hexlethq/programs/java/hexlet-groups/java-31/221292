package exercise.service;

import exercise.HttpClient;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;
import exercise.repository.CityRepository;
import exercise.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
@RequestMapping("http://weather/api/v2/cities")
public class WeatherService {

    @Autowired
    CityRepository cityRepository;
    // Клиент
    public HttpClient client;

    // При создании класса сервиса клиент передаётся снаружи
    // В теории это позволит заменить клиент без изменения самого сервиса
    WeatherService(HttpClient client) {
        this.client = client;
    }

    @GetMapping(path = "/{city}")
    public Optional<Map<String, String>> getWeather(City city) {
        return Optional.ofNullable(client.generateWeather(city.getName()));
    }
}
