package exercise;

import java.util.HashMap;
import java.util.Map;

public class App {

    public static Map<String, Integer> getWordCount(String sentence1) {
        Map<String, Integer> wordsAndCount = new HashMap<>();
        if (sentence1.length() == 0) {
            return wordsAndCount;
        }
        String[] arr = sentence1.split(" ");
        for (String s : arr) {
            Integer wordCount = wordsAndCount.getOrDefault(s, 0);
            wordsAndCount.put(s, wordCount + 1);
        }
        return wordsAndCount;
    }

    public static String toString(Map<String, Integer> wordsAndCount) {
        if (wordsAndCount.size() == 0) {
            return "{}";
        }
        StringBuilder result = new StringBuilder("{\n");
        for (Object key : wordsAndCount.keySet()) {
            result.append("  ").append(key).append(": ").append(wordsAndCount.get(key)).append("\n");
        }
        result.append("}");
        return result.toString();
    }
}