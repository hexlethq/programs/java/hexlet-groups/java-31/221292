package exercise.controller;

import exercise.model.Person;
import exercise.repository.PersonRepository;
import netscape.javascript.JSObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/people")
public class PeopleController {

    // Автоматически заполняем значение поля
    @Autowired
    private PersonRepository personRepository;

    @GetMapping(path = "/{id}")
    public Person getPerson(@PathVariable long id) {
        return this.personRepository.findById(id);
    }

    @GetMapping(path = "")
    public Iterable<Person> getPeople() {
        return this.personRepository.findAll();
    }

    @PostMapping(path = "")
    public void addPerson(@RequestBody Map<String, Object> person) {
        Person p = new Person();
        p.setFirstName(String.valueOf(person.get("firstName")));
        p.setLastName(String.valueOf(person.get("lastName")));
        this.personRepository.save(p);
    }

    @DeleteMapping(path = "/{id}")
    public void deletePerson(@PathVariable long id) {
        this.personRepository.deleteById(id);
    }

    @PatchMapping(path = "/{id}")
    public void updatePerson(@RequestBody Map<String, Object> person, @PathVariable("id") long id) {
        Person p = this.personRepository.findById(id);
        p.setFirstName(String.valueOf(person.get("firstName")));
        p.setLastName(String.valueOf(person.get("lastName")));
        this.personRepository.save(p);
    }
}
