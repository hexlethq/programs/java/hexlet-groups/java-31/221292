package exercise.controller;

import exercise.model.Comment;
import exercise.model.Post;
import exercise.repository.CommentRepository;
import exercise.repository.PostRepository;
import exercise.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/posts")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    @GetMapping(path = "/{postId}/comments")
    public Iterable<Comment> getAllComments(@PathVariable Long postId) {
        return this.commentRepository.findAllByPostId(postId);
    }

    @GetMapping(path = "/{postId}/comments/{commentId}")
    public Comment getCommentsByCommentId(@PathVariable Long postId, @PathVariable Long commentId) throws ResourceNotFoundException {
        return commentRepository.findByIdAndPostId(postId, commentId)
                .orElseThrow(() -> new ResourceNotFoundException("Post" + commentId + "not found"));
    }

    @PostMapping(path = "/{postId}/comments")
    public void createCommentByPostId(@RequestBody Comment comment, @PathVariable Long postId) {
        postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Post" + postId + "not found"));
        comment.setPost(this.postRepository.findById(postId).get());
        this.commentRepository.save(comment);
    }

    @PatchMapping(path = "/{postId}/comments/{commentId}")
    public void updateComment(@PathVariable Long postId, @PathVariable Long commentId, @RequestBody Comment comment) {
        postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Post" + postId + "not found"));
        commentRepository.findById(commentId)
                .orElseThrow(() -> new ResourceNotFoundException("Comment" + commentId + "not found"));
        comment.setId(commentId);
        comment.setPost(postRepository.findById(postId).get());
        this.commentRepository.save(comment);
    }

    @DeleteMapping(path = "/{postId}/comments/{commentId}")
    public void deleteComment(@PathVariable Long postId, @PathVariable Long commentId) {
        postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Post" + postId + "not found"));
        commentRepository.findByIdAndPostId(postId, commentId)
                .orElseThrow(() -> new ResourceNotFoundException("Comment" + commentId + "not found"));
        this.commentRepository.deleteById(commentId);
    }
}
