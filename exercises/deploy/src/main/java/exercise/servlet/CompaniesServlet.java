package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {
        PrintWriter out = response.getWriter();
        StringBuilder result = new StringBuilder();
        if (request.getQueryString() != null && request.getQueryString().contains("search")) {
            String check = request.getParameter("search");
            for (Object company : getCompanies()) {
                if (company.toString().contains(check)) result.append(company).append("\n");
            }
            if (result.toString().equals("") && check != null) {
                result = new StringBuilder("Companies not found");
            }
        } else if (request.getQueryString() == null
                || !request.getQueryString().contains("search")
                || request.getParameter("search") == null) {
            for (Object company : getCompanies()) {
                {
                    result.append(company).append("\n");
                }
            }
        }
        out.println(result);
    }
}
