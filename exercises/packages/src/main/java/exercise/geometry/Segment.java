package exercise.geometry;

public class Segment {

    public static double[][] makeSegment(double[] first, double[] second) {
        double[][] segment = new double[2][2];
        segment[0][0] = first[0];
        segment[0][1] = first[1];
        segment[1][0] = second[0];
        segment[1][1] = second[1];
        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        return new double[]{(int) segment[0][0], (int) segment[0][1]};
    }

    public static double[] getEndPoint(double[][] segment) {
        return new double[]{(int) segment[1][0], (int) segment[1][1]};
    }
}