package exercise;

import exercise.geometry.*;

import java.util.Arrays;

public class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        double first = (segment[0][0] + segment[1][0]) / 2;
        double second = (segment[0][1] + segment[1][1]) / 2;
        return new double[]{first, second};
    }

    public static double[][] reverse(double[][] segment) {
        double[] point1 = Point.makePoint(segment[1][0], segment[1][1]);
        double[] point2 = Point.makePoint(segment[0][0], segment[0][1]);
        double[][] result = Segment.makeSegment(point1, point2);;
        return result;
    }

    public static boolean isBelongToOneQuadrant(double[][] segment){
        if(getQuadrant(segment[0]) == getQuadrant(segment[1])){
            return true;
        }
        else{
            return false;
        }
    }

    public static int getQuadrant(double[] p){
        int result = 0;
        if(p[1] > 0 && p[0] > 0){
            return 1;
        }
        else if(p[1]  > 0 && p[0] < 0){
            return 2;
        }
        else if(p[1]  < 0 && p[0] < 0){
            return 3;
        }
        else if(p[1] < 0 && p[0] > 0){
            return 4;
        }

        return result;
    }
}
