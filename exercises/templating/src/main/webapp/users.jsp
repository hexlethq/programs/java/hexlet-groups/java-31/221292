<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>

<html>
<head>
    <meta charset="UTF-8">
    <title>JSP Application</title>
</head>
<body>
<%List<HashMap<String, String>> users = (List<HashMap<String, String>>) request.getAttribute("users");%>
<table>
    <% for (int i = 0; i < users.size(); i++) {%>
    <tr>
        <td><a href=/users/show?id=<%= users.get(i).get("id")%>> <%= users.get(i).get("id")%> <%= users.get(i).get("firstName")%>  <%= users.get(i).get("lastName")%> <%= users.get(i).get("email")%></a></td>
    </tr>
    <%}%>
</table>
</body>
</html>