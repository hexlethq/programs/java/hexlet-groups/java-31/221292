<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.Map" %>

<html>
<head>
    <meta charset="UTF-8">
    <title>JSP Application</title>
</head>
<body>
<%Map<String, String> user = (Map<String, String>) request.getAttribute("user");%>
<table>
    <tr>
        <td><%= user.get("id")%></td>
        <td><%= user.get("firstName")%></td>
        <td><%= user.get("lastName")%></td>
        <td><%= user.get("email")%></td>
    </tr>
    <tr>
    <td><a href=/users/delete?id=<%= user.get("id")%>> Delete </a></td>
    </tr>
</table>
</body>
</html>
