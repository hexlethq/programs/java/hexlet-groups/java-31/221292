package exercise;

class App {
    public static String getAbbreviation(String str) {
        String result = "";
        result += str.charAt(0);
        for(int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' '){
                result += str.charAt(i + 1);
            }
        }
        return result.toUpperCase();
    }
}
