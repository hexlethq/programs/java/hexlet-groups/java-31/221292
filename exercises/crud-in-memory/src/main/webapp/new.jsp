<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add new user</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
            crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <a href="${pageContext.request.contextPath}/users">Все пользователи</a>
            <form action="${pageContext.request.contextPath}/users/new" method="post">
                <div class="mb-3">
                    <label for="firstName"></label><input id="firstName" class="form-control" type="text" name="firstName" placeholder="first name">
                    <label for="lastName"></label><input id="lastName" class="form-control" type="text" name="lastName" placeholder="last name">
                    <label for="email"></label><input id="email" class="form-control" type="text" name="email" placeholder="email">
                </div>
                <button class="btn btn-primary" type="submit">Создать</button>
            </form>
        </div>
    </body>
</html>
