package exercise;

class App {
    public static void numbers() {
        System.out.print((8 / 2) + (100 % 3));
    }

    public static void strings() {
        String language = "Java";
        System.out.println("Java " + "works on JVM");
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        System.out.println(soldiersCount + " " + name);
    }
}
