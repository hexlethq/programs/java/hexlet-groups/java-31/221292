package exercise.controller;

import com.querydsl.core.types.Predicate;
import exercise.model.User;
import exercise.model.QUser;
import exercise.repository.UserRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;

// Зависимости для самостоятельной работы
// import org.springframework.data.querydsl.binding.QuerydslPredicate;
// import com.querydsl.core.types.Predicate;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserRepository userRepository;

    @Operation(summary = "Get list of all users")
    @ApiResponse(responseCode = "200", description = "List of all users")
    @GetMapping(path = "")
    public Iterable<User> getUsers(
            Model user,
            @RequestParam(value = "firstName", required = false) String firstName,
            @RequestParam(value = "lastName", required = false) String lastName,
            @QuerydslPredicate(root = User.class) Predicate predicate,
            Pageable pageable
    ) {
        user.addAttribute("users", userRepository.findAll(predicate, pageable));

        if (firstName != null && lastName != null) {
            return this.userRepository.findAll(QUser.user.firstName.toLowerCase()
                    .contains(firstName.toLowerCase())
                    .and(QUser.user.lastName.toLowerCase().contains(lastName.toLowerCase())));
        }
        if (firstName != null && lastName == null) {
            return this.userRepository.findAll(QUser.user.firstName.toLowerCase()
                    .contains(firstName.toLowerCase()));
        }

        if (firstName == null && lastName != null) {
            return this.userRepository.findAll(QUser.user.lastName.toLowerCase()
                    .contains(lastName.toLowerCase()));
        }
        return this.userRepository.findAll();
    }
}
