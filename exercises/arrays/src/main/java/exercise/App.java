package exercise;

import java.util.Arrays;

class App {
    public static int[] reverse(int[] arr) {
        if (arr.length == 0) {
            return arr;
        } else {
            int[] result = new int[arr.length];
            for (int i = arr.length - 1, j = 0; i >= 0; i--, j++)
                result[j] = arr[i];
            return result;
        }
    }

    public static int mult(int[] arr) {
        if (arr.length == 0) {
            return 1;
        } else {
            int result = arr[0];
            for (int i = 1; i < arr.length; i++) {
                result *= arr[i];
            }
            return result;
        }
    }

    public static int[] flattenMatrix(int[][] matrix) {
        int k = 0;
        int[] result = new int[size(matrix)];
        if (matrix.length == 0) {
            return result;
        } else {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    result[k] += matrix[i][j];
                    k++;
                }
            }

        }
        return result;
    }

    public static int size(int[][] arr){
        int result = 0;
        for (int i = 0; i < arr.length; i ++){
            result += arr[i].length;
        }
        return result;
    }
}
