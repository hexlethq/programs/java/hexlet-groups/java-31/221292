package exercise;

import exercise.daytimes.Daytime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {

    @Autowired
    MyApplicationConfig config;

    Meal meal = new Meal();


    @GetMapping("/daytime")
    public String root() {
        String daytime = config.getTimeOfTheDay().getName();
        String m = meal.getMealForDaytime(daytime);
        if (daytime.equals("morning")) {
            return "It is morning now. Enjoy your " + m;
        }
        if (daytime.equals("day")) {
            return "It is day now. Enjoy your " + m;
        }
        if (daytime.equals("evening")) {
            return "It is evening now. Enjoy your " + m;
        }
        return "It is night now. Enjoy your supper";
    }
}
