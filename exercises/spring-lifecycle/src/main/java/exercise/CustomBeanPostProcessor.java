// Класс для самостоятельной работы

package exercise;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;


@Component
public class CustomBeanPostProcessor implements BeanPostProcessor{

    public Object postProcessBeforeInitialization(Object bean, String beanName){
        System.out.println("Called postProcessBeforeInitialization for bean: " + beanName);
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName){
        System.out.println("Called postProcessAfterInitialization for bean: " + beanName);
        return bean;
    }
}
