package exercise;

import exercise.connections.Connected;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

public class TcpConnection implements Connection {
    private final String ip;
    private final int port;
    private Object state;

    public TcpConnection(String ipvalue, int portvalue) {
        this.ip = ipvalue;
        this.port = portvalue;
    }


    @Override
    public final Object getCurrentState() {
        return state;
    }

    @Override
    public final void connect() {
        new Connected(this).connect();

    }

    @Override
    public final void disconnect() {
        new Disconnected(this).disconnect();
    }

    @Override
    public final void write(String str) {
        new Connected(this).write(str);
    }

    public final void setState(Connected connected) {
        state = "connected";
    }

    public final void setState(Disconnected disconnected) {
        state = "disconnected";
    }
}

