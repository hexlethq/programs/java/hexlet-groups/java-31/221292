package exercise.connections;

import exercise.TcpConnection;

public class Connected implements Connection {

    private TcpConnection tcpConnection;

    public Connected(TcpConnection connection) {
        this.tcpConnection = connection;
    }

    @Override
    public final Object getCurrentState() {
        return "connected";
    }

    @Override
    public final void connect() {
        if ("connected".equals(tcpConnection.getCurrentState())) {
            System.out.println("Error! Connection already disconnected");
        }
        tcpConnection.setState(new Connected(tcpConnection));
    }

    @Override
    public final void disconnect() {
        tcpConnection.setState(new Disconnected(tcpConnection));
    }

    @Override
    public final void write(String str) {
        if ("disconnected".equals(tcpConnection.getCurrentState())) {
            System.out.println("Error! Connection already disconnected");
        }
    }
}
