package exercise.connections;

public interface Connection {
    String STATE = null;

    default Object getCurrentState() {
        return STATE;
    }

    void connect() throws Exception;

    void disconnect() throws Exception;

    void write(String str);
}
