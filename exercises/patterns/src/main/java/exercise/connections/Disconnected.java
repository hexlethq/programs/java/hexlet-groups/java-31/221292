package exercise.connections;

import exercise.TcpConnection;

public class Disconnected implements Connection {

    private final TcpConnection tcpConnection;

    public Disconnected(TcpConnection connection) {
        this.tcpConnection = connection;
    }

    @Override
    public final Object getCurrentState() {
        return "disconnected";
    }

    @Override
    public final void connect() {
        tcpConnection.setState(new Connected(tcpConnection));
    }

    @Override
    public final void disconnect() {
        if ("disconnected".equals(tcpConnection.getCurrentState())) {
            System.out.println("Error! Connection already disconnected");
        }
        tcpConnection.setState(new Disconnected(tcpConnection));
    }

    @Override
    public final void write(String str) {
        if ("disconnected".equals(tcpConnection.getCurrentState())) {
            System.out.println("Error! Connection already disconnected");
        }
    }
}
