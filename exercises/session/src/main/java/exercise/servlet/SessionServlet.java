package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;

import java.util.Map;
import java.util.logging.Level;

import static exercise.App.getUsers;

import exercise.Users;

public class SessionServlet extends HttpServlet {

    private Users users = getUsers();

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        if (request.getRequestURI().equals("/login")) {
            showLoginPage(request, response);
            return;
        }

        response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
            throws IOException, ServletException {

        switch (request.getRequestURI()) {
            case "/login":
                login(request, response);
                break;
            case "/logout":
                logout(request, response);
                break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private void showLoginPage(HttpServletRequest request,
                               HttpServletResponse response)
            throws IOException, ServletException {

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
        requestDispatcher.forward(request, response);
    }

    private void login(HttpServletRequest request,
                       HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();
        boolean validate = false;
        for (Map<String, String> user : getUsers().getAll()) {
            if (user.get("email").equals(request.getParameter("email"))
                    && request.getParameter("password").equals("password")) {
                session.setAttribute("userId", user.get("id"));
                session.setAttribute("flash", "Вы успешно вошли");
                validate = true;
            }
        }

        if (validate == true) {
            response.sendRedirect("/");

        } else {
            session.setAttribute("email", request.getParameter("email"));
            session.setAttribute("flash", "Неверные логин или пароль");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
            response.setStatus(422);
            requestDispatcher.forward(request, response);
            return;
        }
    }


    private void logout(HttpServletRequest request,
                        HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();
        session.removeAttribute("userId");
        session.removeAttribute("flash");
        session.setAttribute("flash", "Вы успешно вышли");
        response.sendRedirect("/");
    }
}
