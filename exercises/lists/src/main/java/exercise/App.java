package exercise;

import java.util.ArrayList;
import java.util.Collections;

class App {

    public static boolean scrabble(String collection, String word) {
        boolean result = false;
        //Список букв для проверки
        ArrayList wordByLetters = convertArrayToList(collection.toLowerCase().split(""));
        //Список букв слова
        ArrayList letters = convertArrayToList(word.toLowerCase().split(""));
        for (Object o : wordByLetters) {
            letters.remove(o);
        }
        if (letters.size() == 0) {
            result = true;
        }
        return result;
    }

    public static ArrayList convertArrayToList(String[] array) {
        ArrayList result = new ArrayList();
        Collections.addAll(result, array);
        return result;
    }
}