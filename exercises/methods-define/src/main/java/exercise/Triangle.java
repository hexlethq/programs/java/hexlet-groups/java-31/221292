package exercise;


class Triangle {
    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }

    public static double getSquare(int first, int second, int ugol) {
        return ((first * second) / 2) * Math.sin(ugol * Math.PI / 180);
    }
}

