package exercise;

class Converter {
    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }

    public static int convert(int value, String conv) {
        switch (conv) {
            case "b":
                return value * (int) Math.pow(2.0,10.0);
            case "Kb":
                return value / (int) Math.pow(2.0,10.0);
            default:
                return 0;
        }
    }
}

