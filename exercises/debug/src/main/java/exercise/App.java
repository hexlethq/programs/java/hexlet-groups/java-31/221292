package exercise;

class App {
    public static String getTypeOfTriangle(int first, int second, int third) {
        boolean exist = exist(first, second, third);
        boolean equilateral = equilateral(first, second, third);
        boolean versatile = versatile(first, second, third);
        boolean isosceles = isosceles(first, second, third);
        if (exist && equilateral) {
            return "Равносторонний";
        } else if (exist && versatile) {
            return "Разносторонний";
        } else if (exist && isosceles) {
            return "Равнобедренный";
        } else {
            return "Треугольник не существует";
        }
    }

    //Проверяет существует ли треугольник
    public static boolean exist(int first, int second, int third) {
        return (first + second > third && second + third > first && first + third > second)
                && (first > 0 && second > 0 && third > 0);
    }

    //Проверяет равносторонний ли треугольник
    public static boolean equilateral(int first, int second, int third) {
        return first == second && second == third;
    }

    //Проверяет разносторонний ли треугольник
    public static boolean versatile(int first, int second, int third) {
        return first != second && second != third && first != third;
    }

    //Проверяет равнобедренный ли треугольник
    public static boolean isosceles(int first, int second, int third) {
        return (first == second && second != third) ||
                (first == third && third != second) ||
                (second == third && first != second);
    }

    public static int getFinalGrade(int exam, int project) {
        if(exam > 90 || project > 10){
            return 100;
        }
        else if(exam > 75 && project >= 5){
            return 90;
        }
        else if(exam > 50 && project >= 2){
            return 75;
        }
        else{
            return 0;
        }
    }
}