package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

class App {

    public static String[][] enlargeArrayImage(String[][] image) {
        return Arrays.stream(image)
                .map(x -> process(x))
                .flatMap(x -> Stream.of(x, x))
                .toArray(String[][]::new);
    }

    public static String[] process(String[] arr) {
        return Stream.of(arr)
                .flatMap(x -> Stream.of(x, x))
                .toArray(String[]::new);
    }
}
