package exercise;

import java.util.Map;

class FileKV implements KeyValueStorage {
    String path;

    public FileKV(String path, Map<String, String> map) {
        this.path = path;
        Utils.writeFile(path, Utils.serialize(map));
    }

    @Override
    public void set(String key, String value) {
        Map<String, String> result = toMap();
        result.put(key, value);
        Utils.writeFile(path, Utils.serialize(result));
    }

    @Override
    public void unset(String key) {
        Map<String, String> result = toMap();
        result.remove(key);
        Utils.writeFile(path, Utils.serialize(result));
    }

    @Override
    public Object get(String key, String defaultValue) {
        return toMap().getOrDefault(key, defaultValue);
    }

    public Map<String, String> toMap() {
        return Utils.unserialize(Utils.readFile(path));
    }
}
