package exercise;

import java.util.Map.Entry;

class App {
    public static KeyValueStorage swapKeyValue(KeyValueStorage obj) {
        for (Entry<String, String> entry : obj.toMap().entrySet()) {
            obj.set(entry.getValue(), entry.getKey());
            obj.unset(entry.getKey());
        }
        return obj;
    }
}
