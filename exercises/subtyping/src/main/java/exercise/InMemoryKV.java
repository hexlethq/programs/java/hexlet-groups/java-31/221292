package exercise;

import java.util.Map;
import java.util.HashMap;

class InMemoryKV implements KeyValueStorage {

    HashMap<String, String> map;

    public InMemoryKV(Map<String, String> map) {
        this.map = new HashMap<>(Map.copyOf(map));
    }

    @Override
    public void set(String key, String value) {
        map.put(key, value);
    }

    @Override
    public void unset(String key) {
        map.remove(key);
    }

    @Override
    public Object get(String key, String defaultValue) {
        return map.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(Map.copyOf(map));
    }
}
