package exercise.controller;

import exercise.model.Course;
import exercise.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseRepository courseRepository;

    @GetMapping(path = "")
    public Iterable<Course> getCorses() {
        return courseRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Course getCourse(@PathVariable long id) {
        return courseRepository.findById(id);
    }

    //возвращает список всех курсов, которые нужно пройти перед этим курсом, в виде JSON
    @GetMapping(path = "/{id}/previous")
    public List<Course> getPreviousCourse(@PathVariable long id) {
        String path = courseRepository.findById(id).getPath();
        if (path == null) {
            return new ArrayList<>();
        }
        List<Long> p = Arrays.stream(path.split("\\."))
                .mapToLong(Long::parseLong).boxed().collect(Collectors.toList());
        return (List<Course>) courseRepository.findAllById(p);
    }


}
