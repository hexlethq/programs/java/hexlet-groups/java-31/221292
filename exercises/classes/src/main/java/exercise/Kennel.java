package exercise;

import java.util.ArrayList;
import java.util.Arrays;

public class Kennel {

    public static int puppyCount;
    public static ArrayList<String[]> puppies = new ArrayList<>(0);

    public static void addPuppy(String[] puppy) {
        puppies.add(puppy);
        puppyCount++;
    }

    public static void addSomePuppies(String[][] puppies) {
        for (String[] puppy : puppies) {
            addPuppy(puppy);
        }
    }

    public static int getPuppyCount() {
        return puppyCount;
    }

    public static boolean isContainPuppy(String name) {
        for (String[] puppy : puppies) {
            if (puppy[0].equals(name)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        String[][] result = new String[puppies.size()][2];
        for (int i = 0; i < result.length; i++) {
            result[i] = puppies.get(i);
        }
        return result;
    }

    public static String[] getNamesByBreed(String breed) {
        int resultsize = 0;
        for (String[] puppy : puppies) {
            if (puppy[1].equals(breed)) {
                resultsize++;
            }
        }
        String[] result = new String[resultsize];
        for (int i = 0, j = 0; i < puppies.size(); i++) {
            if (puppies.get(i)[1].equals(breed)) {
                result[j] = puppies.get(i)[0];
                j++;
            }
        }
        return result;
    }

    public static void resetKennel(){
        puppies.clear();
        puppyCount = 0;
    }
}

