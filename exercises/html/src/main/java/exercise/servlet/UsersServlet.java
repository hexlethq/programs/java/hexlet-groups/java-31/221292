package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.*;

import java.nio.file.Paths;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }
        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");
        showUser(request, response, id);
    }

    private List getUsers() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        List<Object> result;
        result = Arrays.asList(mapper.readValue
                (Paths.get("src/main/resources/users.json")
                        .toFile(), Object[].class));
        return result;
    }

    private void showUsers(HttpServletRequest request,
                           HttpServletResponse response)
            throws IOException {
        List<LinkedHashMap> result = getUsers();
        for (LinkedHashMap obj : result) {
            response.setContentType("text/html;charset=UTF-8");
            String body = "<html><body><table>" +
                    "<tr>" +
                    "<td>" + (obj).get("id") + "</td>" +
                    "<td><a href=/users/" + (obj).get("id") + ">" + (obj).get("firstName")
                    + " " + (obj).get("lastName") + "</a></td>" +
                    "</tr></table>" +
                    "</body></html>";
            PrintWriter out = response.getWriter();
            out.println(body);
        }
    }

    private void showUser(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        List<LinkedHashMap> result = getUsers();
        boolean idcheck = false;


        for (LinkedHashMap obj : result) {
            if ((obj).get("id").equals(id)) {
                idcheck = true;
                response.setContentType("text/html;charset=UTF-8");
                String body = "<html><body><table>" +
                        "<tr>" +
                        "<td>" + (obj).get("id") + "</td>" +
                        "<td>" + (obj).get("firstName") + "</td>" +
                        "<td>" + (obj).get("lastName") + "</td>" +
                        "<td>" + (obj).get("email") + "</td>" +
                        "</tr></table>" +
                        "</body></html>";
                PrintWriter out = response.getWriter();
                out.println(body);
            }
        }
        if (!idcheck) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
