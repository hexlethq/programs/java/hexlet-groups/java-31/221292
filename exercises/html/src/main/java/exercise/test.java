package exercise;

import java.util.Comparator;
import java.util.PriorityQueue;

public class test {
    public static void main(String[] args) {
        int[] arr = new int[]{110, 90, 40, 70, 80, 30, 10, 20, 50, 60, 65, 31, 29, 11, 9};
        PriorityQueue<Integer> MaxHeap = new PriorityQueue<>(Comparator.reverseOrder());
        for (int i = 0; i < arr.length; i++){
            MaxHeap.add(arr[i]);
        }
        System.out.println(MaxHeap);
    }
}
