package exercise;


class Point {

    public static int[] makePoint(int x, int y) {
        int[] p = {x, y};
        return p;
    }

    public static String pointToString(int[] p){
        String result = "";
        result = "(" + String.valueOf(getX(p)) + ", " + String.valueOf(getY(p)) + ")";
        return result;
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static int getQuadrant(int[] p){
        int result = 0;
        if(getY(p) > 0 && getX(p) > 0){
            return 1;
        }
        else if(getY(p) > 0 && getX(p) < 0){
            return 2;
        }
        else if(getY(p) < 0 && getX(p) < 0){
            return 3;
        }
        else if(getY(p) < 0 && getX(p) > 0){
            return 4;
        }

        return result;
    }

    public static int[] getSymmetricalPointByX(int[] p) {
        if (getY(p) == 0) {
            return makePoint(getX(p), getY(p));
        }
        else if (getY(p) < 0) {
            return makePoint(getX(p), Math.abs(getY(p)));
        }
        else if (getY(p) > 0) {
            return makePoint(getX(p), -1 * getY(p));
        }
        return p;
    }

    public static int calculateDistance(int[] first, int[] second){
        //AB = √AC2 + BC2.
        int result = 0;
        result = (int) Math.sqrt(Math.pow((second[0] - first[0]), 2)
                + Math.pow((second[1] - first[1]), 2));

        return result;
    }
}
