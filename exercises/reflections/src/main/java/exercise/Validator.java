package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Validator {


    public static List<String> validate(Address address) {
        List<Field> fields = List.of(address.getClass().getDeclaredFields());
        List<String> result = new ArrayList<>();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                if (field.isAnnotationPresent(NotNull.class) && field.get(address) == null) {
                    result.add(field.getName());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> result = new HashMap<>();
        List<Field> fields = List.of(address.getClass().getDeclaredFields());
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                if (field.isAnnotationPresent(NotNull.class) && field.get(address) == null) {
                    List<String> message = new ArrayList<>();
                    message.add("can not be null");
                    result.put(field.getName(), message);
                }
                if (field.isAnnotationPresent(MinLength.class)
                        && field.getAnnotation(MinLength.class).minLength() > field.get(address).toString().length()) {
                    List<String> message = new ArrayList<>();
                    message.add("length less than 4");
                    result.put(field.getName(), message);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}