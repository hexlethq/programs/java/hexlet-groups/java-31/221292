package exercise;

class Address {
    @NotNull
    @MinLength(minLength = 4)
    final String country;

    @NotNull
    final String city;

    @NotNull
    final String street;

    @NotNull
    final String houseNumber;

    final String flatNumber;

    Address(String country, String city, String street, String houseNumber, String flatNumber) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
    }
}
