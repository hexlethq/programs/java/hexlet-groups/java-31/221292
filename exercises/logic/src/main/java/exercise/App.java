package exercise;

class App {
    public static boolean isBigOdd(int number) {
        boolean isBigOddVariable;
        if(number % 2 != 0 && number >= 1001){
            isBigOddVariable = true;
        }
        else {
            isBigOddVariable = false;
        }
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        if(number % 2 == 0){
            System.out.println("yes");
        }
        else{
            System.out.println("no");
        }
    }

    public static void printPartOfHour(int minutes) {
        if(minutes >= 0 && minutes <= 14){
            System.out.println("First");
        }
        else if(minutes >= 15 && minutes <= 30){
            System.out.println("Second");
        }
        else if(minutes >= 31 && minutes <= 45){
            System.out.println("Third");
        }
        else if(minutes >= 46 && minutes <= 59){
            System.out.println("Fourth");
        }
    }
}
