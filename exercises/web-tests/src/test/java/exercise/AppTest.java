package exercise;

import kong.unirest.JsonNode;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import io.javalin.Javalin;
import io.ebean.DB;

import exercise.domain.User;
import exercise.domain.query.QUser;
import io.ebean.Database;

class AppTest {

    private static Javalin app;
    private static String baseUrl;

    @BeforeAll
    public static void beforeAll() {
        app = App.getApp();
        app.start(0);
        int port = app.port();
        baseUrl = "http://localhost:" + port;
    }

    @AfterAll
    public static void afterAll() {
        app.stop();
    }

    // Между тестами база данных очищается
    // Благодаря этому тесты не влияют друг на друга
    @BeforeEach
    void beforeEach() {
        Database db = DB.getDefault();
        db.truncate("user");
        User existingUser = new User("Wendell", "Legros", "a@a.com", "123456");
        existingUser.save();
    }

    @Test
    void testUsers() {

        // Выполняем GET запрос на адрес http://localhost:port/users
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users")
            .asString();
        // Получаем тело ответа
        String content = response.getBody();

        // Проверяем код ответа
        assertThat(response.getStatus()).isEqualTo(200);
        // Проверяем, что страница содержит определенный текст
        assertThat(response.getBody()).contains("Wendell Legros");
    }

    @Test
    void testNewUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/new")
            .asString();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void testCreateUser() {
        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .header("Content-Type", "application/json")
                .field("firstName", "Aleksandr")
                .field("lastName", "Beloff")
                .field("email", "albel@hotmail.com")
                .field("password", "12344321")
                .asEmpty();
        assertThat(responsePost.getStatus()).isEqualTo(302);
        User actualUser = new QUser()
                .lastName.equalTo("Beloff")
                .findOne();
        assertThat(actualUser).isNotNull();
        assertThat(actualUser.getFirstName()).isEqualTo("Aleksandr");
        assertThat(actualUser.getLastName()).isEqualTo("Beloff");
        assertThat(actualUser.getEmail()).isEqualTo("albel@hotmail.com");
        assertThat(actualUser.getPassword()).isEqualTo("12344321");
    }

    @Test
    void testCreateUserWithEmptyFirstName() {
        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .header("Content-Type", "application/json")
                .field("firstName", "")
                .field("lastName", "Beloff")
                .field("email", "albel@hotmail.com")
                .field("password", "12344321")
                .asEmpty();
        assertThat(responsePost.getStatus()).isEqualTo(422);
        assertThat(responsePost.getBody().contains("Имя не должно быть пустым")).isTrue();
        User actualUser = new QUser()
                .lastName.equalTo("Beloff")
                .findOne();
        assertThat(actualUser).isNull();
    }

    @Test
    void testCreateUserWithEmptyLastName() {
        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .header("Content-Type", "application/json")
                .field("firstName", "Aleksandr")
                .field("lastName", "")
                .field("email", "albel@hotmail.com")
                .field("password", "12344321")
                .asEmpty();
        assertThat(responsePost.getStatus()).isEqualTo(422);
        assertThat(responsePost.getBody().contains("Фамилия не должна быть пустой")).isTrue();
        User actualUser = new QUser()
                .lastName.equalTo("Beloff")
                .findOne();
        assertThat(actualUser).isNull();
    }

    @Test
    void testCreateUserWithIncEmail() {
        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .header("Content-Type", "application/json")
                .field("firstName", "Aleksandr")
                .field("lastName", "Beloff")
                .field("email", "albelhotmailcom")
                .field("password", "12344321")
                .asEmpty();
        assertThat(responsePost.getStatus()).isEqualTo(422);
        assertThat(responsePost.getBody().contains("Должно быть валидным email")).isTrue();
        User actualUser = new QUser()
                .lastName.equalTo("Beloff")
                .findOne();
        assertThat(actualUser).isNull();
    }

    @Test
    void testCreateUserWithIncPass() {
        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .header("Content-Type", "application/json")
                .field("firstName", "Aleksandr")
                .field("lastName", "Beloff")
                .field("email", "albel@hotmail.com")
                .field("password", "123")
                .asEmpty();
        assertThat(responsePost.getStatus()).isEqualTo(422);
        assertThat(responsePost.getBody().contains("Пароль должен содержать не менее 4 символов")).isTrue();
        User actualUser = new QUser()
                .lastName.equalTo("Beloff")
                .findOne();
        assertThat(actualUser).isNull();
    }
}
