package exercise;

import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
class User {
    int id;
    String firstName;
    String lastName;
    int age;
}
