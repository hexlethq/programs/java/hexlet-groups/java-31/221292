package exercise;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.Paths;

class App {

    public static void save(Path path, Car car) throws JsonProcessingException {
        String result = car.serialize();
        try (FileWriter writer = new FileWriter(String.valueOf(path), false)) {
            writer.write(result);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static Car extract(Path path) throws IOException {
        Path fullPath = Paths.get(String.valueOf(path)).toAbsolutePath().normalize();
        String content = "";
        try {
            content = Files.readString(fullPath);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return Car.unserialize(content);
    }
}
