package exercise;

import java.util.List;
import java.util.Map;

class Tag {
    String type;
    Map<String, String> attributes;

    public Tag(String type, Map<String, String> attributes) {
        this.type = type;
        this.attributes = attributes;
    }

    public String toString() {
        List<String> attrKeys = attributes.keySet().stream().toList();
        List<String> attrValues = attributes.values().stream().toList();
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("<").append(type).append(" ");
        for (int i = 0; i < attrKeys.size(); i++) {
            strBuilder.append(attrKeys.get(i)).append("=").append("\"").append(attrValues.get(i)).append("\" ");
        }
        strBuilder.deleteCharAt(strBuilder.length() - 1);
        strBuilder.append(">");
        return String.valueOf(strBuilder);
    }
}
