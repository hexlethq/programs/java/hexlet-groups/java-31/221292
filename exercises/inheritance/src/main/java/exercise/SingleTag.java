package exercise;

import java.util.Map;

class SingleTag extends Tag{

    String type;
    Map<String, String> attributes;

    public SingleTag(String type, Map<String, String> attributes) {
        super(type, attributes);
        this.type = type;
        this.attributes = attributes;
    }
}
