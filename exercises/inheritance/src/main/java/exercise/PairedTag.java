package exercise;

import java.util.Map;
import java.util.List;

class PairedTag extends Tag {
    String body;
    List<Tag> children;

    public PairedTag(String type, Map<String, String> attributes, String body, List<Tag> children) {
        super(type, attributes);
        this.type = type;
        this.attributes = attributes;
        this.body = body;
        this.children = children;
    }

    public String toString() {
        return addChildren(new StringBuilder(super.toString()));
    }

    public String addChildren(StringBuilder strBuilder){
        for (Tag child : children) {
            strBuilder.append(child.toString());
        }
        strBuilder.append(body).append("</").append(type).append(">");
        return String.valueOf(strBuilder);
    }
}
