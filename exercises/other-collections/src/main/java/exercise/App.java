package exercise;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class App {
    public static Map<String, String> genDiff(Map<String, Object> first, Map<String, Object> second) {
        return Stream.concat(first.keySet().stream(), second.keySet().stream())
                .collect(Collectors.toSet())
                .stream()
                .collect(Collectors.toMap(p -> p, p -> compare(first, second, p)));
    }

    public static String compare(Map<String, Object> first, Map<String, Object> second, Object key) {
        if (!first.containsKey(key) && second.containsKey(key)) {
            return "added";
        } else if (first.containsKey(key) && !second.containsKey(key)) {
            return "deleted";
        } else if (first.containsKey(key) && second.containsKey(key) && first.get(key).equals(second.get(key))) {
            return "unchanged";
        }
        return "changed";
    }
}
