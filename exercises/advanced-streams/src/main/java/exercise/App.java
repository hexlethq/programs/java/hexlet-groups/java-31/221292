package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

class App {

    public static String getForwardedVariables(String str) {
        Stream<String> streamFromValues = Stream.of(str.split("\n"));
        return Arrays.stream(streamFromValues
                .filter(x -> x.startsWith("environment"))
                .map(x -> x.replaceAll("\"", " "))
                .map(x -> x.replaceAll(",", " "))
                .flatMap((p) -> Arrays.stream(p.split(" "))).toArray(String[]::new))
                .filter(x -> x.startsWith("X_FORWARDED_"))
                .map(x -> x.replaceAll("X_FORWARDED_", ""))
                .sequential()
                .collect(Collectors.joining(","));
    }
}
