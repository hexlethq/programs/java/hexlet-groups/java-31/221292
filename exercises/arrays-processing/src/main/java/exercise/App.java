package exercise;

class App {

    public static int getIndexOfMaxNegative(int[] arr) {
        int result = -1;
        for (int i = 0; i < arr.length; i++) {
            if (result == -1 && arr[i] < 0) {
                result = i;
            }
            if (arr[i] < 0 && arr[i] > arr[result]) {
                result = i;
            }
        }
        return result;
    }

    public static int[] getElementsLessAverage(int[] arr) {
        if (arr.length == 0) {
            return arr;
        }
        double average = calculateArithmeticMean(arr);
        int[] result = new int[findResultArrayLength(arr)];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (arr[i] < average) {
                result[j] = arr[i];
                j++;
            }
        }
        return result;
    }

    public static double calculateArithmeticMean(int[] arr) {
        double result = 0;
        for (int j : arr) {
            result += j;
        }
        return result / arr.length;
    }

    public static int findResultArrayLength(int[] arr) {
        int j = 0;
        for (int k : arr) {
            if (k < calculateArithmeticMean(arr)) {
                j++;
            }
        }
        return j;
    }

    public static int getSumBeforeMinAndMax(int[] arr) {
        int result = 0;
        int startindex = 0;
        int finishinfdex = 0;
        if (getMinIndex(arr) < getMaxIndex(arr)){
            startindex = getMinIndex(arr);
            finishinfdex = getMaxIndex(arr);
        }
        else if(getMinIndex(arr) > getMaxIndex(arr)){
            startindex = getMaxIndex(arr);
            finishinfdex = getMinIndex(arr);
        }
        else if(getMinIndex(arr) == getMaxIndex(arr)){
            return result;
        }
        for (int i = startindex + 1; i < finishinfdex; i++) {
            result += arr[i];
        }
        return result;
    }

    public static int getMinIndex(int[] arr) {
        int result = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[result]) {
                result = i;
            }
        }
        return result;
    }

    public static int getMaxIndex(int[] arr) {
        int result = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > arr[result]) {
                result = i;
            }
        }
        return result;
    }
}
