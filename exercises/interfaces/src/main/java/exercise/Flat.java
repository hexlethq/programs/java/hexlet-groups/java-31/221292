package exercise;

public class Flat implements Home {
    double area;
    double balconyArea;
    int floor;

    public Flat(double area, double balconyArea, int floor) {
        this.area = area;
        this.balconyArea = balconyArea;
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "Квартира площадью " + getArea() + " метров на " + getFloor() + " этаже";

    }

    @Override
    public int compareTo(Home another) {
        int res = 0;
        if (another.getArea() > getArea()) {
            res = -1;
        } else if (getArea() > another.getArea()) {
            res = 1;
        }
        return res;
    }

    public double getArea() {
        return area + balconyArea;
    }

    public int getFloor() {
        return floor;
    }
}
