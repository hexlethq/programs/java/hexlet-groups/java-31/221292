package exercise;

class ReversedSequence implements CharSequence {

    String str;

    public ReversedSequence(String str) {
        StringBuilder buffer = new StringBuilder(str);
        this.str = String.valueOf(buffer.reverse());
    }

    @Override
    public int length() {
        return str.length();
    }

    @Override
    public String toString() {
        return str;
    }

    @Override
    public char charAt(int index) {
        return str.charAt(index - 1);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return str.subSequence(start, end);
    }
}
