package exercise;

public class Cottage implements Home {
    double area;
    int floorCount;

    public Cottage(double area, int floorCount) {
        this.area = area;
        this.floorCount = floorCount;
    }

    @Override
    public String toString() {
        return getFloorCount() + " этажный коттедж площадью " + getArea() + " метров";
    }

    @Override
    public int compareTo(Home another) {
        int res = 0;
        if (another.getArea() > getArea()) {
            res = -1;
        } else if (getArea() > another.getArea()) {
            res = 1;
        }
        return res;
    }

    public double getArea() {
        return area;
    }

    public int getFloorCount() {
        return floorCount;
    }
}
