package exercise;

import java.util.*;

public class App {
    public static List<String> buildAppartmentsList(List<Home> appartments, int elcont) {
        return appartments.stream()
                .sorted(Home::compareTo)
                .map(Home::toString)
                .limit(elcont)
                .toList();
    }
}
