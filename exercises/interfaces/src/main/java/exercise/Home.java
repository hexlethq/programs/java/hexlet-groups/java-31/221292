package exercise;

interface Home {

    int compareTo(Home another);

    String toString();

    double getArea();
}
