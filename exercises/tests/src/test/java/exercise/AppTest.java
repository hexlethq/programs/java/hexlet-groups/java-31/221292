package exercise;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class AppTest {

    @Test
    void countLessSizeTest() {
        List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        Assertions.assertEquals( 2, App.take(numbers1, 2).size());
        Assertions.assertEquals(1, App.take(numbers1, 2).get(0));
        Assertions.assertEquals(2, App.take(numbers1, 2).get(1));
    }

    @Test
    void countMoreSizeTest() {
        List<Integer> numbers2 = new ArrayList<>(Arrays.asList(7, 3, 10));
        Assertions.assertEquals( 3, App.take(numbers2, 8).size());
        Assertions.assertEquals( 7, App.take(numbers2, 8).get(0));
        Assertions.assertEquals( 3, App.take(numbers2, 8).get(1));
        Assertions.assertEquals( 10, App.take(numbers2, 8).get(2));
    }
    @Test
    void countZeroTest() {
        List<Integer> numbers2 = new ArrayList<>(Arrays.asList(7, 3, 10));
        Assertions.assertEquals( 0, App.take(numbers2, 0).size());
    }

    @Test
    void sizeZeroTest() {
        List<Integer> numbers3 = new ArrayList<>(Arrays.asList());
        Assertions.assertEquals( 0, App.take(numbers3, 8).size());
    }

    @Test
    void reusableСallTest() {
        List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        Assertions.assertEquals( 0, App.take(numbers1, 0).size());
        Assertions.assertEquals( 1, App.take(numbers1, 1).size());
        Assertions.assertEquals(1, App.take(numbers1, 1).get(0));
        Assertions.assertEquals( 2, App.take(numbers1, 2).size());
        Assertions.assertEquals(1, App.take(numbers1, 2).get(0));
        Assertions.assertEquals(2, App.take(numbers1, 2).get(1));
    }
}
